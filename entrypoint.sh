#!/bin/sh

if [[ "x$COLS" == "x" ]] ; then
    #echo "\$COLS is undefined. Use 'docker run -e COLS=\$(stty size | awk {'print \$2'}) <image_name>' to get graphics"
    ./centerGraphic.sh -c "100" -s tanuki.txt && ./centerGraphic.sh -c "100" text.txt
else
    ./centerGraphic.sh -c "$COLS" -s tanuki.txt && ./centerGraphic.sh -c "$COLS" text.txt
    echo ""
fi
